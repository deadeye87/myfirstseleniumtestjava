import com.thoughtworks.selenium.Selenium;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class testBasketPage {
    WebDriver driver;
    homePage homepage;
    basketPage basketpage;
    String basketUrl = "https://www.bol.com/nl/order/basket.html";

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver", "c:\\webdriver\\geckodriver.exe");
        FirefoxOptions browserOptions = new FirefoxOptions();
        browserOptions.setCapability("marionette", true);
        driver.manage().window().maximize();
        driver = new FirefoxDriver();
        homepage = new homePage(driver);
        basketpage = new basketPage((driver));
    }
    @Test
    public void testCheckIfBasketReturnsValidUrl() throws Exception {


        basketpage.navToBasket();
        Assert.assertEquals(basketUrl, driver.getCurrentUrl() );


    }

    @After
    public void teardown() throws Exception {
        if (driver != null) {
            driver.quit();
        }
    }
}
