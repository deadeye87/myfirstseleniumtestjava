import com.thoughtworks.selenium.Selenium;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;


public class testHomePage {
    WebDriver driver;
    homePage homepage;

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver", "c:\\webdriver\\geckodriver.exe");
        FirefoxOptions browserOptions = new FirefoxOptions();
        browserOptions.setCapability("marionette", true);
        driver.manage().window().maximize();
        driver = new FirefoxDriver();
        homepage = new homePage(driver);
    }
    @Test
    public void testCheckIfProductIsInBasket() throws Exception {
        homepage.open();
        homepage.openBasket();
    }
    @Test
    public void testPlaceProductInBasket() throws InterruptedException {
        homepage.open();
        homepage.placeProductInBasket();
    }


    @After
    public void teardown() throws Exception {
        if (driver != null) {
            driver.quit();
        }
}
}
