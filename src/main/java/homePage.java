import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Scanner;

public class homePage {
    final WebDriver driver;


    public homePage(WebDriver driver) {
        this.driver = driver;

    }

    public void open() {
        driver.navigate().to("https://www.bol.com/nl/nl/");
    }

    //Locator for cookie btn
    By cookie_btn = By.cssSelector("button.ui-btn:nth-child(1)");
    //locator for searchbar
    By searchbar = By.id("searchfor");
    //locator for razor_product
    By product = By.className("product-title");
    //locator for basket
    By orderProduct = By.linkText("In winkelwagen");
    // locator for order
    By order = By.cssSelector("a.ui-btn--primary");
    //locator for basket
    By basket = By.id("basket");

    //method for accepting cookies
    public void clickCookieBtn() {
        driver.findElement(cookie_btn).click();
    }
    //method for searchbar with hardcoded input
    public void input_Searchbar() {
        driver.findElement(searchbar)
                .sendKeys("Razer Basilisk V2 RGB - Optische Gaming Muis - 20000 DPI - Zwart", Keys.ENTER);
    }
    //method for clicking on product
    public void clickProduct() {
        driver.findElement(product).click();
    }
    //method for placing product in basket
    public void OrderProduct() {
        driver.findElement(orderProduct).click();
    }
    //method to click on order for placing product in your basket
    public void clickOrder() {
        driver.findElement(order).click();
    }
    //method to open your Basket
    public void openBasket() {
        driver.findElement(basket).click();
    }
    public void placeProductInBasket() throws InterruptedException {
        //methode to enter a product in the searchbar and placing it in your basket.

        clickCookieBtn();
        Thread.sleep(1500);
        input_Searchbar();
        Thread.sleep(1500);
        clickProduct();
        Thread.sleep(1500);
        OrderProduct();
        Thread.sleep(1500);
        clickOrder();
    }

}
